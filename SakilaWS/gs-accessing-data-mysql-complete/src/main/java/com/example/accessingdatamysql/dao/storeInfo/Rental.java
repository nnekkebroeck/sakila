package com.example.accessingdatamysql.dao.storeInfo;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="rental")
public class Rental {
	@Id
	@GeneratedValue
	@Column(name="rental_id", nullable=false)
	private Long rentalId;
	@Column(name="rental_date", nullable=false)
	private Date rentalDate;
	@Column(name="inventory_id", nullable=false)
	private Long inventoryId;
	@Column(name="customer_id", nullable=false)
	private Long customerId;
	@Column(name="return_date", nullable=false)
	private Date returnDate;
	@Column(name="staff_id", nullable=false)
	private int staffId;
	@Column(name="last_update", nullable=false)
	private Timestamp last_update;
	
	public Long getRentalId() {
		return rentalId;
	}
	public void setRentalId(Long rentalId) {
		this.rentalId = rentalId;
	}
	public Date getRentalDate() {
		return rentalDate;
	}
	public void setRentalDate(Date rentalDate) {
		this.rentalDate = rentalDate;
	}
	public Long getInventoryId() {
		return inventoryId;
	}
	public void setInventoryId(Long inventoryId) {
		this.inventoryId = inventoryId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	public int getStaffId() {
		return staffId;
	}
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}
	public Timestamp getLast_update() {
		return last_update;
	}
	public void setLast_update(Timestamp last_update) {
		this.last_update = last_update;
	}
}
