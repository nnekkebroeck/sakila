package com.example.accessingdatamysql.business;

import java.util.Optional;

import com.example.accessingdatamysql.dao.customerInfo.Address;
import com.example.accessingdatamysql.dao.storeInfo.Staff;

public class StoreComplete {
	private int storeId;
	private Optional<Address> address;
	private Optional<Staff> staffMember;
	
	public int getStoreId() {
		return storeId;
	}
	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	public Optional<Staff> getStaffMember() {
		return staffMember;
	}
	public void setStaffMember(Optional<Staff> staffMember) {
		this.staffMember = staffMember;
	}
	public void setAddress(Optional<Address> address2) {
		this.address = address2;
	}
	
	public Optional<Address> getAddress() {
		return address;
	}
}
