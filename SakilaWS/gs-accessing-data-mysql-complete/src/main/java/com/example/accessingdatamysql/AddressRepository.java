package com.example.accessingdatamysql;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.example.accessingdatamysql.dao.customerInfo.Address;

public interface AddressRepository extends CrudRepository<Address, Integer> {
	public Iterable<Address> findAll();
	public Optional<Address> findById(Integer id);
}
