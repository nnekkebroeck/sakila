package com.example.accessingdatamysql.dao.movie;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="language")
public class Language {
	@Id
	@GeneratedValue
	@Column(name="language_id", nullable=false)
	private Long languageId;
	@Column(name="name", nullable=false)
	private String name;
	@Column(name="last_update", nullable=false)
	private Timestamp lastUpdate;
}
