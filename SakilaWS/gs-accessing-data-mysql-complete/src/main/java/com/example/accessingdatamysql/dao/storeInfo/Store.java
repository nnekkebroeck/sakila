package com.example.accessingdatamysql.dao.storeInfo;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
	
@Entity
@Table
public class Store {
	@Id
	@GeneratedValue
	@Column(name="store_id", nullable=false)
	private int storeId;
	@Column(name="manager_staff_id", nullable=false)
	private Integer managerStaffId;
	@Column(name="address_id")
	private Integer addressId;
	@Column(name="last_update", nullable=false)
	private Timestamp last_update;
	
	public int getStoreId() {
		return storeId;
	}
	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	public Integer getManagerStaffId() {
		return managerStaffId;
	}
	public void setManagerStaffId(Integer managerStaffId) {
		this.managerStaffId = managerStaffId;
	}
	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}
	public Timestamp getLast_update() {
		return last_update;
	}
	public void setLast_update(Timestamp last_update) {
		this.last_update = last_update;
	}
}
