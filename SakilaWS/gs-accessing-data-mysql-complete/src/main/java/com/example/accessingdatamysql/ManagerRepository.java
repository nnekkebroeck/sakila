package com.example.accessingdatamysql;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.example.accessingdatamysql.dao.customerInfo.Address;
import com.example.accessingdatamysql.dao.storeInfo.Staff;

public interface ManagerRepository extends CrudRepository<Staff, Integer> {
	public Iterable<Staff> findAll();
	public Optional<Staff> findById(Integer id);
}
