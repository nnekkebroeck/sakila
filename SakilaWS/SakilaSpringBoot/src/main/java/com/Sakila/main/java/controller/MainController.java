package com.Sakila.main.java.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;

import com.Sakila.dao.storeInfo.Store;
import com.Sakila.dao.storeInfo.StoreDAO;

@Controller
public class MainController {
	 @Autowired
	 private StoreDAO storeDAO;
	 
	    @RequestMapping(value = "/", method = RequestMethod.GET)
	    public String showBankAccounts(Model model) {
	        ArrayList<Store> list = storeDAO.getListOfStores();
	 
	        model.addAttribute("storeList", list);
	 
	        return "storesPage";
	    }
}
