package com.Sakila.dao.movie;

import java.sql.Clob;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="film")
public class Film {
	@Id
	@GeneratedValue
	@Column(name="film_id", nullable=false)
	private Long filmId;
	@Column(name="title", nullable=false)
	private String title;
	@Column(name="description")
	private String description;
	@Column(name="release_year", nullable=false)
	private int releaseYear;
	@Column(name="language_id", nullable=false)
	private Long languageId;
	@Column(name="original_language_id", nullable=false)
	private Long originalLanguageId;
	@Column(name="rental_duration", nullable=false)
	private int rentalDuration;
	@Column(name="rental_rate", nullable=false)
	private double rentalRate;
	@Column(name="length", nullable=false)
	private int length;
	@Column(name="rental_cost", nullable=false)
	private double rentalCost;
	@Column(name="rating", nullable=false)
	private String rating;
	@Column(name="special_features", nullable=false)
	private Clob specialFeatures;
	@Column(name="last_update", nullable=false)
	private Timestamp lastUpdate;
	public Long getFilmId() {
		return filmId;
	}
	public void setFilmId(Long filmId) {
		this.filmId = filmId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getReleaseYear() {
		return releaseYear;
	}
	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}
	public Long getLanguageId() {
		return languageId;
	}
	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}
	public Long getOriginalLanguageId() {
		return originalLanguageId;
	}
	public void setOriginalLanguageId(Long originalLanguageId) {
		this.originalLanguageId = originalLanguageId;
	}
	public int getRentalDuration() {
		return rentalDuration;
	}
	public void setRentalDuration(int rentalDuration) {
		this.rentalDuration = rentalDuration;
	}
	public double getRentalRate() {
		return rentalRate;
	}
	public void setRentalRate(double rentalRate) {
		this.rentalRate = rentalRate;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public double getRentalCost() {
		return rentalCost;
	}
	public void setRentalCost(double rentalCost) {
		this.rentalCost = rentalCost;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public Clob getSpecialFeatures() {
		return specialFeatures;
	}
	public void setSpecialFeatures(Clob specialFeatures) {
		this.specialFeatures = specialFeatures;
	}
	public Timestamp getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
