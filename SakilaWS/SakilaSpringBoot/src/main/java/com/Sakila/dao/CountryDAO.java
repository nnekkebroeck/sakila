package com.Sakila.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.Sakila.dao.customerInfo.City;

@Repository
@Transactional
public class CountryDAO {
	private SessionFactory sessionFactory;
	
	 public City findById(Long id) {
	    Session session = this.sessionFactory.getCurrentSession();
	    return session.get(City.class, id);
	 }
	 
	 public List<City> listBankAccountInfo() {
	        String sql = "Select new " + City.class.getName() //
	                + "(e.id,e.city,e.country_id, e.last_update) " //
	                + " from " + City.class.getName() + " e ";
	        Session session = this.sessionFactory.getCurrentSession();
	        Query<City> query = session.createQuery(sql, City.class);
	        return query.getResultList();
	    }
}
