package com.Sakila.dao.movie;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="film_category")
public class FilmCategory {
	@Id
	@GeneratedValue
	@Column(name="film_id", nullable=false)
	private Long filmId;
	@Column(name="category_id", nullable=false)
	private Long categoryId;
	@Column(name="last_update", nullable=false)
	private Timestamp last_update;
	
}
