package com.Sakila.dao.storeInfo.copy;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="store")
public class Store {
	@Id
	@GeneratedValue
	@Column(name="store_id", nullable=false)
	private Long storeId;
	@Column(name="manager_staff_id", nullable=false)
	private Long managerStaffId;
	@Column(name="adress_id", nullable=false)
	private Long addressId;
	@Column(name="last_update", nullable=false)
	private Timestamp last_update;
	
	public Long getStoreId() {
		return storeId;
	}
	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}
	public Long getManagerStaffId() {
		return managerStaffId;
	}
	public void setManagerStaffId(Long managerStaffId) {
		this.managerStaffId = managerStaffId;
	}
	public Long getAddressId() {
		return addressId;
	}
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}
	public Timestamp getLast_update() {
		return last_update;
	}
	public void setLast_update(Timestamp last_update) {
		this.last_update = last_update;
	}
}
