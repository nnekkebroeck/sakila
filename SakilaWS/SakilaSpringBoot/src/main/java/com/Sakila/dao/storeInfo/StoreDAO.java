package com.Sakila.dao.storeInfo;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class StoreDAO {
	@Autowired
    private SessionFactory sessionFactory;
 
    public StoreDAO() {
    }
 
    public Store findById(int id) {
        Session session = (Session) this.sessionFactory.getCurrentSession();	
        return ((org.hibernate.Session) session).get(Store.class, id);
    }

    public ArrayList<Store> getListOfStores() {
    	String sql = "Select new " + Store.class.getName() //
                + "(e.store_id,e.manager_staff_id,e.adress_id) " //
                + " from " + Store.class.getName() + " e ";
        Session session = (Session) this.sessionFactory.getCurrentSession();
        Query<Store> query = ((org.hibernate.Session) session).createQuery(sql, Store.class);
        return (ArrayList<Store>) query.getResultList();
    }
}
