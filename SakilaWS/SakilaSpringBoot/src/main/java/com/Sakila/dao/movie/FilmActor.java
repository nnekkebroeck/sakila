package com.Sakila.dao.movie;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="film_actor")
public class FilmActor {
	@Id
	@GeneratedValue
	@Column(name="actor_id", nullable=false)
	private Long actorId;
	@Column(name="film_id", nullable=false)
	private Long filmId;
	@Column(name="last_update", nullable=false)
	private Timestamp lastUpdate;
	
	public Long getActorId() {
		return actorId;
	}
	public void setActorId(Long actorId) {
		this.actorId = actorId;
	}
	public Long getFilmId() {
		return filmId;
	}
	public void setFilmId(Long filmId) {
		this.filmId = filmId;
	}
	public Timestamp getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
