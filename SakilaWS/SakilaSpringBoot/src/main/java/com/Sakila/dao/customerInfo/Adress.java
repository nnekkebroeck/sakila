package com.Sakila.dao.customerInfo;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="address")
public class Adress {
	@Id
	@GeneratedValue
	@Column(name="address_id", nullable=false)
	private Long address_id;
	@Column(name="address", length=50, nullable=false)
	private String address;
	@Column(name="address2", length=50, nullable=false)
	private String address2;
	@Column(name="city_id", length=50, nullable=false)
	private Long cityId;
	@Column(name="postal_code", length=50, nullable=false)
	private String postalCode;
	@Column(name="phone", length=50, nullable=false)
	private String phone;
	@Column(name="last_update", nullable=false)
	private Timestamp lastUpdate;
	public Long getAddress_id() {
		return address_id;
	}
	public void setAddress_id(Long address_id) {
		this.address_id = address_id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public Long getCityId() {
		return cityId;
	}
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Timestamp getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
