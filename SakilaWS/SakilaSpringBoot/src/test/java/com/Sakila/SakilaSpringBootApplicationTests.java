package com.Sakila;


import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import org.junit.Assert.*;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import com.Sakila.dao.storeInfo.Store;
import com.Sakila.dao.storeInfo.StoreDAO;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
  classes = { StoreDAO.class }, 
  loader = AnnotationConfigContextLoader.class)
@Transactional

class SakilaSpringBootApplicationTests {

	@Test
	public void testStores() {
		StoreDAO dao = new StoreDAO();
		ArrayList<Store> stores = dao.getListOfStores();
		boolean condition = false;
		if (stores.size() >= 1) {
			condition = true;
		}
		assertTrue(condition);
	}

}
